﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using RequestModel;
using ResponseModel;

namespace Service
{
    public class BankAccountService
    {
        private TransactionProcessingDbContext db;

        public BankAccountService(TransactionProcessingDbContext dbContext)
        {
            db = dbContext;
        }

        public async Task<ResponseViewModel> GetBalance(int accountNumber)
        {
            ResponseViewModel response = null;
            BankAccount account = await db.BankAccounts.FirstOrDefaultAsync(x => x.AccountNumber == accountNumber);
            if (account == null)
            {
                response = CreateErrorResponse(accountNumber, "Account not found");
            }
            else
            {
                Currency currency = await db.Currencies.FirstAsync(x => x.Id == account.CurrencyId);
                response = new ResponseViewModel()
                {
                    AccountNumber = account.AccountNumber,
                    Successful = true,
                    Message = "",
                    Balance = account.Balance,
                    Currency = currency.Code
                };
            }

            return response;
        }

        public async Task<ResponseViewModel> Deposit(DepositRequest request)
        {
            ResponseViewModel response = null;
            if (request.Amount < 0)
            {
                response = CreateErrorResponse(request.AccountNumber, "Amount must be greater than zero (0)");
                return response;
            }

            BankAccount account = await db.BankAccounts.FirstOrDefaultAsync(x => x.AccountNumber == request.AccountNumber);
            if (account == null)
            {
                response = CreateErrorResponse(request.AccountNumber, "Account not found");
                return response;
            }

            Currency currency = await db.Currencies.FirstAsync(x => x.Id == account.CurrencyId);
            Wallet wallet =
                await db.Wallets.FirstOrDefaultAsync(x => x.AccountId == account.Id && x.CurrencyId == currency.Id);
            if (wallet == null)
            {
                response = CreateErrorResponse(account.AccountNumber, "Account wallet not found");
                return response;
            }

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    TransactionLog entity = new TransactionLog()
                    {
                        AccountId = account.Id,
                        Amount = request.Amount,
                        Created = DateTime.Now,
                        CurrencyId = currency.Id,
                        IsActive = true,
                        IsSuccessful = true,
                        Modified = DateTime.Now,
                        WalletId = wallet.Id,
                    };

                    TransactionLog addedLog = db.TransactionLogs.Add(entity);
                    wallet.Balance += request.Amount;
                    account.Balance += ConvertToUsd(currency, request.Amount);
                    await db.SaveChangesAsync();
                    transaction.Commit();

                    response = new ResponseViewModel()
                    {
                        AccountNumber = account.AccountNumber,
                        Balance = account.Balance,
                        Successful = true,
                        Message = "Success",
                        Currency = DefaultCurrency.USD.ToString()
                    };
                }
                catch (Exception exception)
                {
                    response = CreateErrorResponse(account.AccountNumber, exception.Message);
                }
            }

            return response;
        }

        public async Task<ResponseViewModel> Withdraw(WithdrawRequest request)
        {
            ResponseViewModel response = null;
            if (request.Amount < 0)
            {
                response = CreateErrorResponse(request.AccountNumber, "Amount must be greater than zero (0)");
                return response;
            }

            BankAccount account = await db.BankAccounts.FirstOrDefaultAsync(x => x.AccountNumber == request.AccountNumber);
            if (account == null)
            {
                response = CreateErrorResponse(request.AccountNumber, "Account not found");
                return response;
            }

            Currency currency = await db.Currencies.FirstAsync(x => x.Id == account.CurrencyId);
            Wallet wallet =
                await db.Wallets.FirstOrDefaultAsync(x => x.AccountId == account.Id && x.CurrencyId == currency.Id);
            if (wallet == null)
            {
                response = CreateErrorResponse(account.AccountNumber, "Account wallet not found");
                return response;
            }

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    bool shouldDeplete = wallet.Balance > request.Amount;

                    TransactionLog entity = new TransactionLog()
                    {
                        AccountId = account.Id,
                        Amount = request.Amount,
                        Created = DateTime.Now,
                        CurrencyId = currency.Id,
                        IsActive = true,
                        IsSuccessful = shouldDeplete,
                        Modified = DateTime.Now,
                        WalletId = wallet.Id,
                    };

                    TransactionLog addedLog = db.TransactionLogs.Add(entity);

                    if (shouldDeplete)
                    {
                        wallet.Balance -= request.Amount;
                        account.Balance -= ConvertToUsd(currency, request.Amount);
                    }

                    await db.SaveChangesAsync();
                    transaction.Commit();

                    response = new ResponseViewModel()
                    {
                        AccountNumber = account.AccountNumber,
                        Balance = account.Balance,
                        Successful = shouldDeplete,
                        Message = shouldDeplete? "Success": "Insufficient balance",
                        Currency = DefaultCurrency.USD.ToString()
                    };
                }
                catch (Exception exception)
                {
                    response = CreateErrorResponse(account.AccountNumber, exception.Message);
                }
            }

            return response;
        }

        private ResponseViewModel CreateErrorResponse(int accountNumber, string message)
        {
            return new ResponseViewModel()
            {
                AccountNumber = accountNumber,
                Successful = false,
                Message = message,
            };
        }

        private decimal ConvertToUsd(Currency currency, decimal amount)
        {
            return currency.Code == DefaultCurrency.USD.ToString() ? amount : amount / currency.OneUsdEquivalent;
        }
    }
}
