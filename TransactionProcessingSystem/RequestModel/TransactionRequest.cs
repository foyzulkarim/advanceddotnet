﻿namespace RequestModel
{
    public abstract class TransactionRequest
    {
        public int AccountNumber { get; set; }

        public decimal Amount { get; set; }

        public string Currency { get; set; }
    }
}