﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class Currency : Entity
    {
        [Required]
        [MaxLength(10)]
        [Index(IsUnique = true)]
        public string Code { get; set; }

        [Required]
        [Index]
        [MaxLength(24)]
        public string Name { get; set; }

        [Required]
        [Index]
        public decimal OneUsdEquivalent { get; set; }
    }
}