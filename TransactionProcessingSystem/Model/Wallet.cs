﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class Wallet : Entity
    {
        [Required]
        [Index]
        public decimal Balance { get; set; }

        [Required]
        [Index]
        public int AccountId { get; set; }

        [Required]
        [Index]
        public int CurrencyId { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        [ForeignKey("AccountId")]
        public virtual BankAccount Account { get; set; }

        [ForeignKey("CurrencyId")]
        public virtual Currency Currency { get; set; }
    }
}