﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class BankAccount : Entity
    {
        [Required]
        [Index(IsUnique = true)]
        public int AccountNumber { get; set; }

        [Required]
        [MaxLength(128)]
        [Index]
        public string Name { get; set; }

        [Required]
        [Index]
        public decimal Balance { get; set; }

        [Required]
        [Index]
        public int CurrencyId { get; set; }

        [ForeignKey("CurrencyId")]
        public virtual Currency Currency { get; set; }

        public virtual ICollection<Wallet> Wallets { get; set; }
    }
}
