﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class TransactionLog : Entity
    {
        [Required]
        [Index]
        public int AccountId { get; set; }

        [Required]
        [Index]
        public int WalletId { get; set; }

        [Required]
        [Index]
        public int CurrencyId { get; set; }

        [Required]
        [Index]
        public decimal Amount { get; set; }

        [Required]
        public bool IsSuccessful { get; set; }

        [ForeignKey("AccountId")]
        public virtual BankAccount Account { get; set; }

        [ForeignKey("WalletId")]
        public virtual Wallet Wallet { get; set; }

        [ForeignKey("CurrencyId")]
        public virtual Currency Currency { get; set; }
    }
}