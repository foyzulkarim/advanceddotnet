﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class TransactionProcessingDbContext : DbContext
    {
        public TransactionProcessingDbContext() : base("TransactionProcessingConnection")
        {
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;            
        }

        public virtual DbSet<BankAccount> BankAccounts { get; set; }

        public virtual DbSet<Wallet> Wallets { get; set; }

        public virtual DbSet<Currency> Currencies { get; set; }

        public virtual DbSet<TransactionLog> TransactionLogs { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
