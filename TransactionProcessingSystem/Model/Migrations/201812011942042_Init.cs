namespace Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BankAccounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountNumber = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 128),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrencyId = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId)
                .Index(t => t.AccountNumber, unique: true)
                .Index(t => t.Name)
                .Index(t => t.Balance)
                .Index(t => t.CurrencyId)
                .Index(t => t.Created)
                .Index(t => t.Modified);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 10),
                        Name = c.String(nullable: false, maxLength: 24),
                        OneUsdEquivalent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true)
                .Index(t => t.Name)
                .Index(t => t.OneUsdEquivalent)
                .Index(t => t.Created)
                .Index(t => t.Modified);
            
            CreateTable(
                "dbo.Wallets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AccountId = c.Int(nullable: false),
                        CurrencyId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BankAccounts", t => t.AccountId)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId)
                .Index(t => t.Balance)
                .Index(t => t.AccountId)
                .Index(t => t.CurrencyId)
                .Index(t => t.Created)
                .Index(t => t.Modified);
            
            CreateTable(
                "dbo.TransactionLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        WalletId = c.Int(nullable: false),
                        CurrencyId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsSuccessful = c.Boolean(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BankAccounts", t => t.AccountId)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId)
                .ForeignKey("dbo.Wallets", t => t.WalletId)
                .Index(t => t.AccountId)
                .Index(t => t.WalletId)
                .Index(t => t.CurrencyId)
                .Index(t => t.Amount)
                .Index(t => t.Created)
                .Index(t => t.Modified);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TransactionLogs", "WalletId", "dbo.Wallets");
            DropForeignKey("dbo.TransactionLogs", "CurrencyId", "dbo.Currencies");
            DropForeignKey("dbo.TransactionLogs", "AccountId", "dbo.BankAccounts");
            DropForeignKey("dbo.Wallets", "CurrencyId", "dbo.Currencies");
            DropForeignKey("dbo.Wallets", "AccountId", "dbo.BankAccounts");
            DropForeignKey("dbo.BankAccounts", "CurrencyId", "dbo.Currencies");
            DropIndex("dbo.TransactionLogs", new[] { "Modified" });
            DropIndex("dbo.TransactionLogs", new[] { "Created" });
            DropIndex("dbo.TransactionLogs", new[] { "Amount" });
            DropIndex("dbo.TransactionLogs", new[] { "CurrencyId" });
            DropIndex("dbo.TransactionLogs", new[] { "WalletId" });
            DropIndex("dbo.TransactionLogs", new[] { "AccountId" });
            DropIndex("dbo.Wallets", new[] { "Modified" });
            DropIndex("dbo.Wallets", new[] { "Created" });
            DropIndex("dbo.Wallets", new[] { "CurrencyId" });
            DropIndex("dbo.Wallets", new[] { "AccountId" });
            DropIndex("dbo.Wallets", new[] { "Balance" });
            DropIndex("dbo.Currencies", new[] { "Modified" });
            DropIndex("dbo.Currencies", new[] { "Created" });
            DropIndex("dbo.Currencies", new[] { "OneUsdEquivalent" });
            DropIndex("dbo.Currencies", new[] { "Name" });
            DropIndex("dbo.Currencies", new[] { "Code" });
            DropIndex("dbo.BankAccounts", new[] { "Modified" });
            DropIndex("dbo.BankAccounts", new[] { "Created" });
            DropIndex("dbo.BankAccounts", new[] { "CurrencyId" });
            DropIndex("dbo.BankAccounts", new[] { "Balance" });
            DropIndex("dbo.BankAccounts", new[] { "Name" });
            DropIndex("dbo.BankAccounts", new[] { "AccountNumber" });
            DropTable("dbo.TransactionLogs");
            DropTable("dbo.Wallets");
            DropTable("dbo.Currencies");
            DropTable("dbo.BankAccounts");
        }
    }
}
