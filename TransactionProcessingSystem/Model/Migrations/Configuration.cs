using System.Collections.Generic;

namespace Model.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Model.TransactionProcessingDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Model.TransactionProcessingDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            AddCurrencies(context);
            AddAccounts(context);
            AddWallets(context);
        }

        private void AddWallets(TransactionProcessingDbContext context)
        {
            List<Wallet> wallets = new List<Wallet>()
            {
                new Wallet()
                {
                    Id = 1, AccountId = 1, Balance = 0, Created = DateTime.Now, CurrencyId = 1, IsActive = true,
                    Modified = DateTime.Now
                },
                new Wallet()
                {
                    Id = 2, AccountId = 1, Balance = 0, Created = DateTime.Now, CurrencyId = 2, IsActive = true,
                    Modified = DateTime.Now
                },
                new Wallet()
                {
                    Id = 3, AccountId = 2, Balance = 0, Created = DateTime.Now, CurrencyId = 1, IsActive = true,
                    Modified = DateTime.Now
                },
                new Wallet()
                {
                    Id = 4, AccountId = 2, Balance = 0, Created = DateTime.Now, CurrencyId = 2, IsActive = true,
                    Modified = DateTime.Now
                }
            };

            foreach (var wallet in wallets)
            {
                Wallet dbWallet = context.Wallets.FirstOrDefault(
                    x => x.AccountId == wallet.AccountId && x.CurrencyId == wallet.CurrencyId);
                if (dbWallet == null)
                {
                    context.Wallets.Add(wallet);
                    context.SaveChanges();
                }
            }
        }

        private void AddAccounts(TransactionProcessingDbContext context)
        {
            List<BankAccount> accounts = new List<BankAccount>()
            {
                new BankAccount(){ Id = 1, AccountNumber = 1, Balance = 0, Created = DateTime.Now, Modified = DateTime.Now, CurrencyId = 1, IsActive = true, Name = "Account-1"},
                new BankAccount(){ Id = 2, AccountNumber = 2, Balance = 0, Created = DateTime.Now, Modified = DateTime.Now, CurrencyId = 1, IsActive = true, Name = "Account-2"},
                new BankAccount(){ Id = 3, AccountNumber = 3, Balance = 0, Created = DateTime.Now, Modified = DateTime.Now, CurrencyId = 1, IsActive = true, Name = "Account-3"}
            };

            foreach (var bankAccount in accounts)
            {
                BankAccount account = context.BankAccounts.FirstOrDefault(x => x.AccountNumber == bankAccount.AccountNumber);
                if (account == null)
                {
                    context.BankAccounts.Add(bankAccount);
                    context.SaveChanges();
                }
            }
        }

        private void AddCurrencies(TransactionProcessingDbContext context)
        {
            var currencies = new List<Currency>
            {
                new Currency () {Code = DefaultCurrency.USD.ToString(), Name = "US Dollar", OneUsdEquivalent = 1},
                new Currency{Code = "EUR", Name = "Euro", OneUsdEquivalent = (decimal) 0.883568},
                new Currency{Code = "GBP", Name = "British Pound", OneUsdEquivalent = (decimal) 0.784220},
                new Currency{Code = "BDT", Name = "Bangladeshi Taka", OneUsdEquivalent = (decimal) 84.8151},
                new Currency{Code = "THB", Name = "Thai Baht", OneUsdEquivalent =(decimal)  32.9736},
                new Currency{Code = "MYR", Name = "Malaysian Ringgit", OneUsdEquivalent = (decimal) 4.18448},
            };

            foreach (var currency in currencies)
            {
                Currency dbCurrency = context.Currencies.FirstOrDefault(x => x.Code == currency.Code);
                if (dbCurrency == null)
                {
                    dbCurrency = new Currency()
                    {
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        Code = currency.Code,
                        IsActive = true,
                        OneUsdEquivalent = currency.OneUsdEquivalent,
                        Name = currency.Name
                    };
                    context.Currencies.Add(dbCurrency);
                    context.SaveChanges();
                }
            }
        }
    }
}
