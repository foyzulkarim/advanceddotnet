﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Model;
using RequestModel;
using ResponseModel;
using Service;

namespace TransactionProcessing.Web.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        BankAccountService service;

        public AccountController(BankAccountService service)
        {
            this.service = service;
        }

        [Route("Balance")]
        [ActionName("Balance")]
        [HttpGet]
        public async Task<ResponseViewModel> GetBalance(int accountNumber)
        {
            ResponseViewModel model = await service.GetBalance(accountNumber);
            return model;
        }

        [Route("Deposit")]
        [ActionName("Deposit")]
        [HttpPost]
        public async Task<ResponseViewModel> Deposit(DepositRequest request)
        {
            ResponseViewModel model = await service.Deposit(request);
            return model;
        }

        [Route("Withdraw")]
        [ActionName("Withdraw")]
        [HttpPost]
        public async Task<ResponseViewModel> Withdraw(WithdrawRequest request)
        {
            var model = await service.Withdraw(request);
            return model;
        }
    }
}
