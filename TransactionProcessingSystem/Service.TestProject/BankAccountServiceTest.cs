﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Model;
using Moq;
using RequestModel;
using ResponseModel;

namespace Service.TestProject
{
    [TestClass]
    public class BankAccountServiceTest
    {
        [TestMethod]
        public async Task GetBalance_Test()
        {
            var service = new BankAccountService(CreateMockContext().Object);
            ResponseViewModel model = await service.GetBalance(1);
            Assert.IsNotNull(model);
        }

        [TestMethod]
        public async Task Deposit_Test()
        {
            var service = new BankAccountService(CreateMockContext().Object);
            DepositRequest request = new DepositRequest()
            {
                AccountNumber = 1,
                Amount = 10,
                Currency = "USD"
            };

            ResponseViewModel model = await service.Deposit(request);
            Assert.IsNotNull(model);
            Assert.AreEqual(50, model.Balance);
        }

        [TestMethod]
        public async Task Withdraw_Test()
        {
            var service = new BankAccountService(CreateMockContext().Object);
            var request = new WithdrawRequest()
            {
                AccountNumber = 1,
                Amount = 10,
                Currency = "USD"
            };

            ResponseViewModel model = await service.Withdraw(request);
            Assert.IsNotNull(model);
            Assert.AreEqual(30, model.Balance);
        }

        [TestMethod]
        public async Task Withdraw_Unsuccessful_Test()
        {
            var service = new BankAccountService(CreateMockContext().Object);
            var request = new WithdrawRequest()
            {
                AccountNumber = 1,
                Amount = 50,
                Currency = "USD"
            };

            ResponseViewModel model = await service.Withdraw(request);
            Assert.IsNotNull(model);
            Assert.IsFalse(model.Successful);
            Assert.AreEqual(40, model.Balance);
        }

        private Mock<TransactionProcessingDbContext> CreateMockContext()
        {
            var context = new Mock<TransactionProcessingDbContext>();
            PrepareBankAccountMockContext(context);
            PrepareCurrencyMockContext(context);
            PrepareWalletMockContext(context);
            PrepareTransactionLogMockContext(context);
            return context;
        }

        private void PrepareBankAccountMockContext(Mock<TransactionProcessingDbContext> context)
        {
            var data = new List<BankAccount>()
            {
                new BankAccount() {Id = 1, Name = "Acc1", AccountNumber = 1, Balance = 40, CurrencyId = 1},
                new BankAccount() {Id = 2, Name = "Acc2", AccountNumber = 1, Balance = 0, CurrencyId = 2},
                new BankAccount() {Id = 3, Name = "Acc3", AccountNumber = 1, Balance = 0, CurrencyId = 3},
                new BankAccount() {Id = 4, Name = "Acc4", AccountNumber = 1, Balance = 0, CurrencyId = 2}
            }.AsQueryable();

            var mockBankAccountSet = new Mock<DbSet<BankAccount>>();
            mockBankAccountSet.As<IDbAsyncEnumerable<BankAccount>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<BankAccount>(data.GetEnumerator()));

            mockBankAccountSet.As<IQueryable<BankAccount>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<BankAccount>(data.Provider));

            mockBankAccountSet.As<IQueryable<BankAccount>>().Setup(m => m.Expression).Returns(data.Expression);
            mockBankAccountSet.As<IQueryable<BankAccount>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockBankAccountSet.As<IQueryable<BankAccount>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            context.Setup(c => c.BankAccounts).Returns(mockBankAccountSet.Object);
        }

        private void PrepareCurrencyMockContext(Mock<TransactionProcessingDbContext> context)
        {
            var data = new List<Currency>()
            {
                new Currency () {Id = 1, Code = DefaultCurrency.USD.ToString(), Name = "US Dollar", OneUsdEquivalent = 1},
                new Currency{Id = 2, Code = "EUR", Name = "Euro", OneUsdEquivalent = (decimal) 0.883568},
                new Currency{Id = 3, Code = "GBP", Name = "British Pound", OneUsdEquivalent = (decimal) 0.784220},
                new Currency{Id = 4, Code = "BDT", Name = "Bangladeshi Taka", OneUsdEquivalent = (decimal) 84.8151},
                new Currency{Id = 5, Code = "THB", Name = "Thai Baht", OneUsdEquivalent =(decimal)  32.9736},
                new Currency{Id = 6, Code = "MYR", Name = "Malaysian Ringgit", OneUsdEquivalent = (decimal) 4.18448},
            }.AsQueryable();

            var mock = new Mock<DbSet<Currency>>();
            mock.As<IDbAsyncEnumerable<Currency>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<Currency>(data.GetEnumerator()));

            mock.As<IQueryable<Currency>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<Currency>(data.Provider));

            mock.As<IQueryable<Currency>>().Setup(m => m.Expression).Returns(data.Expression);
            mock.As<IQueryable<Currency>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mock.As<IQueryable<Currency>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            context.Setup(c => c.Currencies).Returns(mock.Object);
        }

        private void PrepareWalletMockContext(Mock<TransactionProcessingDbContext> context)
        {
            var data = new List<Wallet>()
            {
                new Wallet() {Id = 1, AccountId = 1, Balance = 40, CurrencyId = 1},
                new Wallet() {Id = 2, AccountId = 1, Balance = 0, CurrencyId = 2},
                new Wallet() {Id = 3, AccountId = 1, Balance = 0, CurrencyId = 3},
                new Wallet() {Id = 4, AccountId = 1, Balance = 0, CurrencyId = 4},
            }.AsQueryable();

            var mock = new Mock<DbSet<Wallet>>();
            mock.As<IDbAsyncEnumerable<Wallet>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<Wallet>(data.GetEnumerator()));

            mock.As<IQueryable<Wallet>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<Wallet>(data.Provider));

            mock.As<IQueryable<Wallet>>().Setup(m => m.Expression).Returns(data.Expression);
            mock.As<IQueryable<Wallet>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mock.As<IQueryable<Wallet>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            context.Setup(c => c.Wallets).Returns(mock.Object);
        }

        private void PrepareTransactionLogMockContext(Mock<TransactionProcessingDbContext> context)
        {
            var data = new List<TransactionLog>()
            {
                new TransactionLog()
                {
                    Id = 1, AccountId = 1, CurrencyId = 1, Amount = 10, IsActive = true, IsSuccessful = true,
                    WalletId = 1
                },
                new TransactionLog()
                {
                    Id = 2, AccountId = 1, CurrencyId = 1, Amount = 10, IsActive = true, IsSuccessful = true,
                    WalletId = 1
                },
                new TransactionLog()
                {
                    Id = 3, AccountId = 1, CurrencyId = 1, Amount = 10, IsActive = true, IsSuccessful = true,
                    WalletId = 1
                },
                new TransactionLog()
                {
                    Id = 4, AccountId = 1, CurrencyId = 1, Amount = 10, IsActive = true, IsSuccessful = true,
                    WalletId = 1
                },
            }.AsQueryable();

            var mock = new Mock<DbSet<TransactionLog>>();
            mock.As<IDbAsyncEnumerable<TransactionLog>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<TransactionLog>(data.GetEnumerator()));

            mock.As<IQueryable<TransactionLog>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<TransactionLog>(data.Provider));

            mock.As<IQueryable<TransactionLog>>().Setup(m => m.Expression).Returns(data.Expression);
            mock.As<IQueryable<TransactionLog>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mock.As<IQueryable<TransactionLog>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            context.Setup(c => c.TransactionLogs).Returns(mock.Object);
        }
    }
}
