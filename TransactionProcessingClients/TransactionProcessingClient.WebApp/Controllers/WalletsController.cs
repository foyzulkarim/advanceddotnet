﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TransactionProcessingClient.WebApp.Models;

namespace TransactionProcessingClient.WebApp.Controllers
{
    public class WalletsController : Controller
    {
        private TransactionProcessingDbEntities db = new TransactionProcessingDbEntities();

        // GET: Wallets
        public async Task<ActionResult> Index()
        {
            var wallets = db.Wallets.Include(w => w.BankAccount).Include(w => w.Currency);
            return View(await wallets.ToListAsync());
        }

        // GET: Wallets/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Wallet wallet = await db.Wallets.FindAsync(id);
            if (wallet == null)
            {
                return HttpNotFound();
            }
            return View(wallet);
        }

        // GET: Wallets/Create
        public ActionResult Create()
        {
            ViewBag.AccountId = new SelectList(db.BankAccounts, "Id", "Name");
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "Code");
            return View();
        }

        // POST: Wallets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Balance,AccountId,CurrencyId,RowVersion,Created,Modified,IsActive")] Wallet wallet)
        {
            if (ModelState.IsValid)
            {
                db.Wallets.Add(wallet);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AccountId = new SelectList(db.BankAccounts, "Id", "Name", wallet.AccountId);
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "Code", wallet.CurrencyId);
            return View(wallet);
        }

        // GET: Wallets/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Wallet wallet = await db.Wallets.FindAsync(id);
            if (wallet == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccountId = new SelectList(db.BankAccounts, "Id", "Name", wallet.AccountId);
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "Code", wallet.CurrencyId);
            return View(wallet);
        }

        // POST: Wallets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Balance,AccountId,CurrencyId,RowVersion,Created,Modified,IsActive")] Wallet wallet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(wallet).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AccountId = new SelectList(db.BankAccounts, "Id", "Name", wallet.AccountId);
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "Code", wallet.CurrencyId);
            return View(wallet);
        }

        // GET: Wallets/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Wallet wallet = await db.Wallets.FindAsync(id);
            if (wallet == null)
            {
                return HttpNotFound();
            }
            return View(wallet);
        }

        // POST: Wallets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Wallet wallet = await db.Wallets.FindAsync(id);
            db.Wallets.Remove(wallet);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
