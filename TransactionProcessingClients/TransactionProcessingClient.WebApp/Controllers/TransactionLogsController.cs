﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TransactionProcessingClient.WebApp.Models;

namespace TransactionProcessingClient.WebApp.Controllers
{
    public class TransactionLogsController : Controller
    {
        private TransactionProcessingDbEntities db = new TransactionProcessingDbEntities();

        // GET: TransactionLogs
        public async Task<ActionResult> Index()
        {
            var transactionLogs = db.TransactionLogs.Include(t => t.BankAccount).Include(t => t.Currency).Include(t => t.Wallet);
            return View(await transactionLogs.ToListAsync());
        }

        // GET: TransactionLogs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransactionLog transactionLog = await db.TransactionLogs.FindAsync(id);
            if (transactionLog == null)
            {
                return HttpNotFound();
            }
            return View(transactionLog);
        }

        // GET: TransactionLogs/Create
        public ActionResult Create()
        {
            ViewBag.AccountId = new SelectList(db.BankAccounts, "Id", "Name");
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "Code");
            ViewBag.WalletId = new SelectList(db.Wallets, "Id", "Id");
            return View();
        }

        // POST: TransactionLogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,AccountId,WalletId,CurrencyId,Amount,IsSuccessful,Created,Modified,IsActive")] TransactionLog transactionLog)
        {
            if (ModelState.IsValid)
            {
                db.TransactionLogs.Add(transactionLog);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AccountId = new SelectList(db.BankAccounts, "Id", "Name", transactionLog.AccountId);
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "Code", transactionLog.CurrencyId);
            ViewBag.WalletId = new SelectList(db.Wallets, "Id", "Id", transactionLog.WalletId);
            return View(transactionLog);
        }

        // GET: TransactionLogs/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransactionLog transactionLog = await db.TransactionLogs.FindAsync(id);
            if (transactionLog == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccountId = new SelectList(db.BankAccounts, "Id", "Name", transactionLog.AccountId);
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "Code", transactionLog.CurrencyId);
            ViewBag.WalletId = new SelectList(db.Wallets, "Id", "Id", transactionLog.WalletId);
            return View(transactionLog);
        }

        // POST: TransactionLogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,AccountId,WalletId,CurrencyId,Amount,IsSuccessful,Created,Modified,IsActive")] TransactionLog transactionLog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transactionLog).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AccountId = new SelectList(db.BankAccounts, "Id", "Name", transactionLog.AccountId);
            ViewBag.CurrencyId = new SelectList(db.Currencies, "Id", "Code", transactionLog.CurrencyId);
            ViewBag.WalletId = new SelectList(db.Wallets, "Id", "Id", transactionLog.WalletId);
            return View(transactionLog);
        }

        // GET: TransactionLogs/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransactionLog transactionLog = await db.TransactionLogs.FindAsync(id);
            if (transactionLog == null)
            {
                return HttpNotFound();
            }
            return View(transactionLog);
        }

        // POST: TransactionLogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TransactionLog transactionLog = await db.TransactionLogs.FindAsync(id);
            db.TransactionLogs.Remove(transactionLog);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
